import React from "react";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";
import { Icon } from "leaflet";
import * as parkData from "./data/solar_panels.json";
import "./App.css";

export const icon_solar = new Icon({
  iconUrl: "/solar-panels.svg",
  iconSize: [35, 35],
});
export const icon_eolic = new Icon({
  iconUrl: "/energia-eolica.svg",
  iconSize: [35, 35],
});

export default function App() {
  const [activePark, setActivePark] = React.useState(null);

  return (
    <Map center={[5, -75]} zoom={6}>
      <TileLayer
        url="https://server.arcgisonline.com/arcgis/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}"
        attribution=""
      />

      {parkData.features.map((park) => (
        <Marker
          key={park.properties.id}
          position={[
            park.geometry.coordinates[1],
            park.geometry.coordinates[0],
          ]}
          onClick={() => {
            setActivePark(park);
          }}
          icon={park.properties.type == "solar" ? icon_solar : icon_eolic}
          /* icon={icon_solar} */
        />
      ))}

      {activePark && (
        <Popup
          className="request-popup"
          position={[
            activePark.geometry.coordinates[1],
            activePark.geometry.coordinates[0],
          ]}
          onClose={() => {
            setActivePark(null);
          }}
        >
          <div>
            <div className="container-fluid header">
              <img src="/logo-minminas.svg" className="m-3" alt=""></img>
            </div>
            <div className="d-flex justify-content-center mt-2 mb-2">
              <div className="">
                <h2 className="item-title">{activePark.properties.name}</h2>
              </div>
            </div>
            <div className="gallery-container-popup">
              <div className="gallery-item-popup">
                <img
                  className="gallery-img shadow-lg"
                  src={activePark.properties.img_1}
                ></img>
              </div>
              <div className="gallery-item-popup">
                <img
                  className="gallery-img shadow-lg"
                  src={activePark.properties.img_2}
                ></img>
              </div>
              <div className="gallery-item-popup">
                <img
                  className="gallery-img shadow-lg"
                  src={activePark.properties.img_3}
                ></img>
              </div>
            </div>
            <div className="mt-4">
              <iframe
                src={activePark.properties.video}
                frameborder="0"
                allowfullscreen
              ></iframe>
            </div>
            <div className="d-flex justify-content-center mt-2 mb-2">
              <button type="button" class="btn color-button">
                Ver Más
              </button>
            </div>
          </div>
        </Popup>
      )}
    </Map>
  );
}
